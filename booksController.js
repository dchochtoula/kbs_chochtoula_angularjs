myBooks.controller('booksController',['$scope','$state', 'bookService', function($scope, $state , bookService){
	$scope.addBookData = function(book){
		bookService.addBook(book)
		    .then(function(addBResponse){
        	console.log(addBResponse);
      		},
      		function(addBError){
        	console.log(addBError);
      		})
      		.catch(angular.noop);
	};

	$scope.updBookData = function(book){
		bookService.updBook(book)
			.then(function(updBResponse){
        	console.log(updBResponse);
      		},
      		function(updBError){
        	console.log(updBError);
      		})
      		.catch(angular.noop);
      };

      $scope.delBookData = function(bid){
      	bookService.delBook(bid)
      		.then(function(delBResponse){
        	console.log(delBResponse);
      		},
      		function(delBError){
        	console.log(delBError);
      		})
      		.catch(angular.noop);
      };

      bookService.getBooks()
        .then(function(GBResponse){
        console.log(GBResponse);
        $scope.bookData = GBResponse.data;
        //console.log(self.authorData);
      },
      function(GBError){
        console.log(GBError);
      })
      .catch(angular.noop);
}]);