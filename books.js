var myBooks = angular.module('myBooks', ['ui.router']);

myBooks.config(['$stateProvider', function($stateProvider){

	$stateProvider
        .state('myBooks',{
                url: '/myBooks',
                views:{
                    'books':{
                        templateUrl:'library.booklist.tpl.html'}
                }
                
        })
	    .state('myBooks.addBook', { //state gia tin prosthikh vivliwn
	    templateUrl: 'library.addBook.tpl.html',
        controller: 'booksController',
        controllerAs: 'booksCtrl'
        })

        .state('myBooks.updBook', { //state gia thn ananewsh ths vashs
        	templateUrl: 'library.updBook.tpl.html',
            controller: 'booksController',
            controllerAs: 'booksCtrl'
        })

        .state('myBooks.delBook', { //state gia thn diagrafh kapoias egrafhs
        	templateUrl:'library.delBook.tpl.html',
            controller: 'booksController',
            controllerAs: 'booksCtrl'
        })

        .state('myBooks.getBooks',{
        	url:'/Book',
        	templateUrl:'library.gbooks.tpl.html',
            controller: 'booksController',
            controllerAs: 'booksCtrl'
        })

}]);