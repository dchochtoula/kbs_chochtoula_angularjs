(function(){
myAuthors.controller('authorController', ['$scope', '$state', 'authorService' , function($scope, $state , authorService){
	//authorService is passed as a dependency using DI
  //$scope.master={}; //
  //var self = this;
  //self.authorData = [ ];//array to store the data from our server
  //self.resAuthorData = {}; 
	$scope.update = function(author) {
      $scope.master = angular.copy(author);
      data = author;
  };

  $scope.addAuthorData = function(author){
    authorService.addAuthor(author)
      .then(function (addResponse){
          $scope.master = "Allo";
          console.log(addResponse);
          },
          function(addError){
            console.log(addError);//edw h ektelesh paei sto error enw ta data pane sto server kanonika
          })
          .catch(angular.noop); //function to ignore the cases when an error occured -
      // for the possibly unhandled rejection error;
  };

  $scope.updateAuthorData = function(auth){
    authorService.updateAuthor(auth)
      .then(function(updResponse){
        console.log(updResponse);
      },
      function(updError){
        console.log(updError);
      })
      .catch(angular.noop);
  };

  $scope.deleteAuthorData = function(delId){
    authorService.deleteAuthor(delId)
          .then(function(delResponse){
        console.log(delResponse);
      },
      function(delError){
        console.log(delError);
      })
      .catch(angular.noop);
  };

  /*$scope.getAuthorData = function(){
    $scope.authorDataTbl = new 
    /*var authObj = function(fname, lname, age){
      this.fname = fname;
      this.lname = lname;
      this.age = age;
    };
    authorService.getAuthors()
      .then(function(GAResponse){
        console.log(GAResponse.data[0]);
        self.authorData = GAResponse;
        console.log(self.authorData);
      },
      function(GAError){
        console.log(GAError);
      })
      .catch(angular.noop);

  };*/

    authorService.getAuthors()
      .then(function(GAResponse){
        console.log(GAResponse);
        $scope.authorData = GAResponse.data;
        //console.log(self.authorData);
      },
      function(GAError){
        console.log(GAError);
      })
      .catch(angular.noop);

}]);
})();