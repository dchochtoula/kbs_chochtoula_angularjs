myLib.factory('LibraryService',['$http', function ($http){

	function getBooks(){
	    return $http.get('https://www.googleapis.com/books/v1/volumes?q=lordoftherings&maxResults=10');
	};

	//function postBook(Book tempB){
	//	return $http.post("http://localhost:64591/Project1/webresources/Book/insert/"+tempB.title+"/"+tempB.pages+"/"+tempB.author);
	//};

	return {
	    getBooks:getBooks
	};
}]);