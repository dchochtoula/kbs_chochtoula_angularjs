myAuthors.factory('authorService', ['$http', function($http){
	var authorData = {}; //object
	var addAuthor = function(author){
		return $http.post('http://localhost:64591/Project1/webresources/Author/insert/'+author.fname+'/'+author.lname+'/'+author.age);
	};

	var updateAuthor = function(auth){
		return $http.put('http://localhost:64591/Project1/webresources/Author/update/'+auth.aid+'/'+auth.aage);
	};

	var deleteAuthor = function(delId){
		return $http.delete('http://localhost:64591/Project1/webresources/Author/delete/'+delId);
	};

	var getAuthors = function(){
		return $http.get('http://localhost:64591/Project1/webresources/Author');
	};

	return{
		addAuthor: addAuthor,
		updateAuthor: updateAuthor,
		deleteAuthor: deleteAuthor,
		getAuthors: getAuthors
	};
}]);