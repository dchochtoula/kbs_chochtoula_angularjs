 myLib.controller('LibraryController',['$scope','async', 'LibraryService', 'bookService', 'authorService', function($scope, async , LibraryService, bookService, authorService){
	var authObj = function(fname, lname, age){
      this.fname = fname;
      this.lname = lname;
      this.age = age;
    };

    var bookObj = function(title, author, pages){
    	this.title = title;
    	this.author = author;
    	this.pages = pages;
    };

	var lib = this;
	lib.gbooks=async;
	lib.gbooksData= lib.gbooks.data.items;
	console.log( lib.gbooksData);
	for(i = 0; i< lib.gbooksData.length; i++){
		var tempBtitle = lib.gbooksData[i].volumeInfo.title;
		var tempBpages = lib.gbooksData[i].volumeInfo.pageCount;
		var tempBauthor = lib.gbooksData[i].volumeInfo.authors[0];
		tempBObj = new bookObj(tempBtitle, tempBauthor,tempBpages);
		bookService.addBook(tempBObj);

		var tempAauthor1 = lib.gbooksData[i].volumeInfo.authors[0].toString();
		var tempAauthor2 = tempAauthor1.split(" ");

		tempAObj = new authObj(tempAauthor2[0], tempAauthor2[1], 40);
		authorService.addAuthor(tempAObj);
	}
	console.log(tempBtitle);
	console.log(tempBpages);
	console.log(tempBauthor);
	//lib.bookData = [ ];
}]);