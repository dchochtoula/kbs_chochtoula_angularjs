myBooks.factory('bookService', ['$http', function($http){
	var addBook = function(book){
		return $http.post('http://localhost:64591/Project1/webresources/Book/insert/'+book.title+'/'+book.pages+'/'+book.author);
	};

	var updBook = function(book){
		return $http.put('http://localhost:64591/Project1/webresources/Book/update/'+book.id+'/'+book.title);
	};

	var delBook = function(bid){
		return $http.delete('http://localhost:64591/Project1/webresources/Book/delete/'+bid);
	};

	var getBooks = function(){
		return $http.get('http://localhost:64591/Project1/webresources/Book');
	};

	return{
		addBook: addBook,
		updBook: updBook,
		delBook: delBook,
		getBooks: getBooks
	};
}]);