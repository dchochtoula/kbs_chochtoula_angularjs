var myAuthors = angular.module('myAuthors',  ['ui.router']);

myAuthors.config(['$stateProvider', function($stateProvider){

	$stateProvider
        .state('myAuthors', {
            url: '/myAuthors',
            views:{
                'author':{
                    templateUrl:'library.authorlist.tpl.html'
                }
            }
        })

	    .state('myAuthors.addAuthor', { //state gia tin prosthikh vivliwn
	       templateUrl: 'library.addAuthor.tpl.html',
         controller : 'authorController',
         controllerAs : 'authorCtlr'
        })

        .state('myAuthors.updAuthor', { //state gia thn ananewsh ths vashs
        	templateUrl: 'library.updAuthor.tpl.html',
            controller: 'authorController',
            controllerAs: 'authorCtlr'

        })

        .state('myAuthors.delAuthor', { //state gia thn diagrafh kapoias egrafhs
        	templateUrl:'library.delAuthor.tpl.html',
            controller: 'authorController',
            controllerAs: 'authorCtlr'
        })

        .state('myAuthors.getAuthors', {
        	templateUrl:'library.showRes.tpl.html',
            controller: 'authorController',
            controllerAs: 'authorCtlr'
        })

}]);