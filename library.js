var myLib = angular.module('myLib', ['myAuthors', 'myBooks', 'ui.router']);

myLib.config(['$stateProvider', '$urlRouterProvider', 
    function($stateProvider, $urlRouterProvider){
	    $stateProvider
        .state("home", {
            url:"",
            views:{
                'main':{
                    template:'<h1>Library</h1>'
                },

            	'gbooks':{
            		templateUrl:'library.gbooks.tpl.html',
                    controller: 'LibraryController',
                    controllerAs: 'libraryCtrl',
                    resolve: {
                        async:['LibraryService', function(LibraryService){
                            return LibraryService.getBooks();
                        }]
                    }
            	}
            }

        })	
}]);